// @flow
const API_KEY = '8VmoLKHCSMVdMS6TfvGnUkX1mJbeMRUt'
const BASE_URL = "https://api.giphy.com/v1/gifs"
const LIMIT = 25
const PICTURES_LIST_URL = `${BASE_URL}/trending?limit=${LIMIT}&rating=G`
const DETAILS_URL = `${BASE_URL}/`

export async function getPictures (page: number = 1): Array<Object> {
  console.log(`GiphyAPI.getPictures(${page})`);
  response = await fetch(`${PICTURES_LIST_URL}&offset=${LIMIT * (page - 1)}&api_key=${API_KEY}`);
  return await response.json();
}

export async function getPictureDetails (id: number): Object {
  console.log(`GiphyAPI.getPictureDetails(${id})`);
  response = await fetch(`${DETAILS_URL}${id}?api_key=${API_KEY}`);
  return await response.json();
}
