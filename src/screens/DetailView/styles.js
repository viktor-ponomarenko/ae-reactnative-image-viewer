import { Dimensions, StyleSheet } from 'react-native'
const { width } = Dimensions.get('window')

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    alignSelf: 'center',
    width: width,
    height: width,
  },
  backButton: {
    position: 'absolute',
    left: 5,
    top: 5,
  },
  spinner: {
    position: 'absolute',
  },
  detailView: {
    position: 'absolute',
    bottom: 10,
    width: 120,
    right: 10,
    flexDirection: 'row',
  },
  detailViewImage: {
    width: 50,
    height: 50,
  },
  filtersDialog: {
    alignSelf: 'center',
    margin: 20,
  },
  filtersDialogContentContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 16,
  },
  filtersDialogItem: {
    margin: 4,
    fontSize: 22,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  hidden: {
    width: 1,
    height: 1,
  },
})
export default styles
