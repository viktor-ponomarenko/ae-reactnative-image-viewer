// @flow
import * as React from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
//import ImageViewer from 'react-native-image-zoom-viewer'
//import { Surface } from "gl-react-native";
//import ImageFilters from "react-native-gl-image-filters";
import Dialog from "react-native-dialog";

import { 
  FILTER_TYPES,
  NONE_TYPE,
  BLUR_TYPE,
  SEPIA_TYPE,
  NEGATIVE_TYPE,
  SATURATION_TYPE,
} from '../../types/filter_types.js';

import styles from './styles'
import DetailsFooter from './components/DetailsFooter';

type Props = {
  imageUrl: string,
  isLoading: boolean,
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
};

class DetailView extends React.PureComponent<Props> {
  state = {
    isApplyFilterDialogVisible: false,
    appliedFilter: NONE_TYPE,
    isLoading: true
  };

  _renderChooseFiltersDialog() {
    return (
      <Dialog.Container 
          visible={this.state.isApplyFilterDialogVisible}
          style={styles.filtersDialogContentContainer}>
          <Dialog.Title>Choose filter</Dialog.Title>
          <View style={styles.filtersDialogContentContainer}>
            {
              FILTER_TYPES.map((item) => {
                return (
                  <TouchableOpacity key={item.type} onPress={() => this._onFilterItemClicked(item)}>
                    <Text style={styles.filtersDialogItem}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                );
              })
            }
          </View>
          <Dialog.Button
              label="Cancel" 
              onPress={this._handleCancel}/>
        </Dialog.Container>
    );
  }

  _showApplyFilterDialog = () => {
    this.setState({ isApplyFilterDialogVisible: true })
  };

  _handleCancel = () => {
    this.setState({ isApplyFilterDialogVisible: false });
  };

  _onFilterItemClicked = (filterType) => {
    this.setState({ 
      isApplyFilterDialogVisible: false,
      appliedFilter: filterType,
    });
  }

  _onImageLoaded = () => {
    this.setState({ isLoading: false });
  }

  _renderImage() {
      const { pictureDetails } = this.props
      const targetImage = pictureDetails.images.fixed_height
      const isImageLoading = this.state.isLoading
      return (
        <View style={styles.imageContainer}>
        {isImageLoading ? <ActivityIndicator size="large"/> : null}
        <Image
          style={ isImageLoading ? styles.hidden : styles.imageStyle }
          source={{uri: targetImage.url}}
          onLoadEnd={this._onImageLoaded} />
        </View>
      );
  }

  _getFilterAttributes() {
    switch (this.state.appliedFilter.type) {
      case BLUR_TYPE.type: 
        return { blur: 1 };
      case SEPIA_TYPE.type:
        return { sepia: 5 };
      case NEGATIVE_TYPE:
        return { negative: 5 };
      case SATURATION_TYPE:
        return { saturation: 5 };
      default:
        return {};
    }
  }

  _renderFilteredImage() {
      const { pictureDetails } = this.props
      const targetImage = pictureDetails.images.fixed_height
      return (
        <Surface
          width={300}
          height={300}
          style={styles.imageContainer}
          ref={ref => (this.image = ref)}>
          <ImageFilters
            width={300}
            height={300}
            {...(this._getFilterAttributes())}
            style={styles.imageStyle}>
            {targetImage.url}
          </ImageFilters>
        </Surface>
      );
  }

  render () {
    const { shareCallback, pictureDetails } = this.props
    // const imagesUrls = [targetImage]
    return (
      <View style={styles.container}>
        {/*It would be great to see here loader, pinch to zoom here and pan: */}
        {/* <Modal style={styles.imageContainer} transparent={true}>
          <ImageViewer imageUrls={imagesUrls} />
        </Modal> */}
        {this._renderImage()}
        <DetailsFooter
          pictureDetails={pictureDetails}
          shareCallback={shareCallback}
          applyFilterCallback={this._showApplyFilterDialog}
        />
        {this._renderChooseFiltersDialog()}
      </View>
    )
  }
}

export default DetailView