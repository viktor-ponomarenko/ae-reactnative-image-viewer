export const NONE_TYPE = {
    name: "Reset",
    type: "NONE",
};

export const BLUR_TYPE = {
    name: "Blur",
    type: "BLUR",
};

export const SEPIA_TYPE = {
    name: "Sepia",
    type: "SEPIA",
};

export const NEGATIVE_TYPE = {
    name: "Negative",
    type: "NEGATIVE",
};

export const SATURATION_TYPE = {
    name: "Saturation",
    type: "SATURATION",
};

export const FILTER_TYPES = [BLUR_TYPE, SEPIA_TYPE, NEGATIVE_TYPE, SATURATION_TYPE, NONE_TYPE];