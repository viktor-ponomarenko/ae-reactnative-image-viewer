import {
  PICTURES_FETCH_REQUESTED,
  PICTURES_FETCH_SUCCESS,
  FETCH_FAILED
} from './actions';

// @flow
const initialState = {
  pictures: [],
  isLoading: true,
  page: 1,
  errorMessage: '',
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload;
  switch (action.type) {
    case PICTURES_FETCH_REQUESTED:
      return {
        ...initialState,
        page: payload.page,
        isLoading: true
      };
    case PICTURES_FETCH_SUCCESS: {
      const newPictures = payload.page == 1 ?
        payload.pictures.data :
        state.pictures.concat(payload.pictures.data);
      return {
        ...initialState,
        pictures: newPictures,
        isLoading: false,
      }
    }
    case FETCH_FAILED:
      return {
        ...initialState,
        errorMessage: payload,
        isLoading: false
      }
    default:
      return state;
  }
}
