// @flow
import * as React from 'react';
import { Share  } from 'react-native';
import DetailView from '../../screens/DetailView';
import { connect } from 'react-redux';
import { fetchPictureDetails } from './actions';
import { selectHiResImage } from './selectors';
import { BLUR_TYPE, SEPIA_TYPE, NEGATIVE_TYPE, SATURATION_TYPE } from '../../types/filter_types.js';

export interface Props {
  navigation: any,
  fetchPictureDetails: Function,
  isLoading: boolean,
  hiResImage: Function,
}
export interface State {
  imageUrl: string,
}

class DetailViewContainer extends React.Component<Props, State> {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: 'transparent',
      position: 'absolute',
      height: 50,
      top: 0,
      left: 0,
      right: 0,
      borderBottomWidth: 0,
    },
    headerTintColor: '#FFF',
  }

  componentDidMount () {
    const { navigation, fetchPictureDetails } = this.props
    const { pictureDetails } = navigation.state.params
    if (!this.props.hiResImage()) {
      fetchPictureDetails(pictureDetails.id)
    }
  }

  share = (imageId: number): void => {
    const { pictureDetails } = this.props.navigation.state.params;
    Share.share({
      url: `${pictureDetails.images.fixed_height.url}`,
      message: `${pictureDetails.images.fixed_height.url}`
    }, {
      // Android only:
      dialogTitle: 'Share gif',
    })
  }

  render () {
    const { pictureDetails } = this.props.navigation.state.params
    const { isLoading } = this.props
    return <DetailView
      pictureDetails={pictureDetails}
      shareCallback={this.share}
      isLoading={isLoading}
    />
  }
}

function bindAction (dispatch) {
  return {
    fetchPictureDetails: imageId => dispatch(fetchPictureDetails(imageId)),
  }
}

const mapStateToProps = state => ({
  hiResImage: () => selectHiResImage(state),
  isLoading: state.detailViewReducer.isLoading,
})

export default connect(mapStateToProps, bindAction)(DetailViewContainer)