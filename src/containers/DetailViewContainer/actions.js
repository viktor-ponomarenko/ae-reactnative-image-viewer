// @flow

import { getPictureDetails } from '../../services/GiphyAPI'
import { FETCH_FAILED } from '../HomeContainer/actions'
import type { ActionWithPayload, ActionWithoutPayload } from '../../types/actions'

export const PICTURE_DETAILS_FETCH_REQUESTED = 'PICTURE_DETAILS_FETCH_REQUESTED'
export const PICTURE_DETAILS_FETCH_SUCCESS = 'PICTURE_DETAILS_FETCH_SUCCESS'

export function pictureIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURE_DETAILS_FETCH_REQUESTED,
  }
}

export function fetchPictureSuccess (imageId: number, hiResImage: string): ActionWithPayload {
  return {
    type: PICTURE_DETAILS_FETCH_SUCCESS,
    payload: hiResImage.data,
  }
}

export function fetchPictureFailed (errorMessage: string): ActionWithPayload {
  return {
    type: FETCH_FAILED,
    payload: errorMessage,
  }
}

export function fetchPictureDetails (imageId: number) {
  return async dispatch => {
    getPictureDetails(imageId)
      .then((data) => {
        dispatch(fetchPictureSuccess(imageId, data));
      })
      .catch((err) => {
        dispatch(fetchPictureFailed(err.message));
      });
  }
}
