import {
  PICTURE_DETAILS_FETCH_REQUESTED,
  PICTURE_DETAILS_FETCH_SUCCESS,
} from './actions';

const initialState = {
  hiResPicture: null,
  isLoading: false,
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload
  switch (action.type) {
    case PICTURE_DETAILS_FETCH_SUCCESS:
        return {
          isLoading: false,
          hiResPicture: payload,
        }
    case PICTURE_DETAILS_FETCH_REQUESTED:
      return {
        ...initialState,
        isLoading: true,
      }
    default:
      return state;
  }
}
