// @flow

export const selectHiResImage = (state: Object) => {
  if (state.detailViewReducer.hiResPicture === null) {
    return null
  }
  return state.detailViewReducer.hiResPicture.images.fixed_height.url
}
