# AgileEngine Test Task
[Task] description

### Build and run
- Install [npm]
- Goto into folder with the project
- Run in terminal `npm install` command
- Configure iOS and Android environment - look at this [guide] (Tab *Building projects with native code*) and this [one]
- Run command `npm run ios` to run app on iOS emulator
- Run command `npm run android` to run app on Android (both on emulators and devices)
- Profit

[task]: <https://agileengine.bitbucket.io/rnBsQqoEDeHMvLfR/>
[npm]: <https://www.npmjs.com/get-npm>
[guide]: <https://facebook.github.io/react-native/docs/getting-started>
[one]: <https://facebook.github.io/react-native/docs/running-on-device>